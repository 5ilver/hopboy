//Using libs SDL, glibc
#include <SDL.h>	//SDL version 2.0
#include <stdlib.h>
#include <stdio.h>

#define SCREEN_WIDTH 640	//window height
#define SCREEN_HEIGHT 480	//window width

int init(int w, int h, int argc, char *args[]);
int width, height;		//used if fullscreen
SDL_Window* window = NULL;	//The window we'll be rendering to
SDL_Renderer *renderer;		//The renderer SDL will use to draw to the screen
int momentum;
//surfaces
static SDL_Surface *screen;
static SDL_Surface *playermap;
static SDL_Surface *background;
int action;
int score;

//textures
SDL_Texture *screen_texture;
int playerx;
int playery;
int step;
int steppos[4] = {0,1,2,1};
int goleft;
int goright;
int goup;
int jump;
int fall;
int ground=340;
int coinx=320-32;
int coiny=240-32;
int spin;
int spindelay=5;
int spinpos[4] = {0,1,2,1};
int wootdelay;

static void move(){
    if (goleft > 0 && goright == 0 && action == 1 && jump==0  && fall==0){
        momentum++;
    } else if (goleft == 0 && goright > 0 && action == 0 && jump==0 && fall==0){
        momentum++;
    } else if (goleft > 0 && goright == 0 && action == 0){
        momentum=momentum-1;
    } else if (goleft == 0 && goright > 0 && action == 1){
        momentum=momentum-1;
    } else if (jump==0 && fall==0 && playery == ground){
        momentum--;
    }
    if (momentum > 20) momentum=20;
    if (momentum <= 0){ 
        momentum=0;
        if (goleft == 0 && goright == 1 ) action = 0;
        if (goleft == 1 && goright == 0 ) action = 1;
    }
    if (action == 0) playerx=playerx+momentum;
    if (action == 1) playerx=playerx-momentum;
    if ( momentum > 0) {
        step=step+1;
    } else {
        step=0;
    }
    if (step>3) step=0;
    if (playerx < -64) playerx = screen->w;
    if (playerx > screen->w) playerx = -64;
    if (goup == 1 && fall == 0 && jump == 0) jump=12+(momentum^3)/2;
    if (goup == 1 && fall == 0 && jump > 0) jump=jump-1;
    if (goup == 0 && jump > 0 ) jump=0;
    if (jump == 0 && playery < ground ) fall++;
    if (fall > 0 && playery >= ground ) fall=0;
    if (jump < 0) jump=0;
    playery=playery-jump+fall;
    if (playery > ground ) playery = ground;
    if (playerx > coinx-50 && playerx < coinx + 50 && playery > coiny-60 && playery < coiny + 60 && wootdelay==0) wootdelay=10;
}

static void draw_player() {
	
	SDL_Rect src;
	SDL_Rect dest;
	src.x = 192*action + steppos[step]*64;
	src.y = 0;
	src.w = 64;
	src.h = 64;
	dest.x = playerx;
	dest.y = playery;
	dest.w = 64;
	dest.h = 64;
	
	SDL_BlitSurface(playermap, &src, screen, &dest);
}

static void draw_coin() {
	
	SDL_Rect src;
	SDL_Rect dest;
    if (wootdelay == 0){
        src.x = 6*64 + spinpos[spin]*64;
        src.y = 0;
        src.w = 64;
        src.h = 64;
        dest.x = coinx;
        dest.y = coiny;
        dest.w = 64;
        dest.h = 64;
        spindelay--;
        if (spindelay == 0 ){
            spindelay=5;
            spin++;
            if (spin > 3) spin = 0;
        }
    }else{
        src.x = 9*64;
        src.y = 0;
        src.w = 64;
        src.h = 64;
        dest.x = playerx;
        dest.y = playery-64;
        dest.w = 64;
        dest.h = 64;
        wootdelay--;
        if (wootdelay==0){
            coinx=1+(rand() % 586);
            coiny=30+(rand() % 300);
        }
    }
    SDL_BlitSurface(playermap, &src, screen, &dest);
}

static void draw_background() {
	
	SDL_Rect src;
	SDL_Rect dest;
	src.x = 0;
	src.y = 0;
	src.w = 640;
	src.h = 480;
	dest.x = 0;
	dest.y = 0;
	dest.w = 640;
	dest.h = 480;
	
	SDL_BlitSurface(background, &src, screen, &dest);
}

int main (int argc, char *args[]) {
    
    	//SDL Window setup
	if (init(SCREEN_WIDTH, SCREEN_HEIGHT, argc, args) == 1) {
		
		return 0;
	}
	SDL_GetWindowSize(window, &width, &height);

    playerx=screen->h/2;
    playery=screen->w/2;
	int sleep = 0;
	int quit = 0;
	Uint32 next_game_tick = SDL_GetTicks();
	//render loop
	while(quit == 0) {
 		//check for new events every frame
		SDL_PumpEvents();

		const Uint8 *keystate = SDL_GetKeyboardState(NULL);
        //const SDL_Event * event;
        //if (event->window.event == SDL_WINDOWEVENT_CLOSE) quit=1;
		if (keystate[SDL_SCANCODE_ESCAPE]) quit = 1;
		if (keystate[SDL_SCANCODE_Q]) quit = 1;
		goleft=0;
        goright=0;
        goup=0;
		if (keystate[SDL_SCANCODE_LEFT]) goleft=1;
		if (keystate[SDL_SCANCODE_RIGHT]) goright=1;
		if (keystate[SDL_SCANCODE_SPACE]) goup=1;
		move();
        
        //draw background
		SDL_RenderClear(renderer);
		SDL_FillRect(screen, NULL, 0x000000ff);
        draw_background();
        // do and draw everything else
        draw_player();
        draw_coin();
        SDL_UpdateTexture(screen_texture, NULL, screen->pixels, screen->w * sizeof (Uint32));
		SDL_RenderCopy(renderer, screen_texture, NULL, NULL);

		//draw to the display
		SDL_RenderPresent(renderer);
				
		//time it takes to render  frame in milliseconds
		next_game_tick += 1000 / 60;
		sleep = next_game_tick - SDL_GetTicks();
	
		if( sleep > 0 ) {
            				
			SDL_Delay(sleep);
		}
    }
    //free loaded images
	SDL_FreeSurface(screen);
	SDL_FreeSurface(playermap);
    SDL_FreeSurface(background);
	//free renderer and all textures used with it
	SDL_DestroyRenderer(renderer);
	
	//Destroy window 
	SDL_DestroyWindow(window);

	//Quit SDL subsystems 
	SDL_Quit(); 
	 
	return 0;
}

int init(int width, int height, int argc, char *args[]) {
	int i;
		//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {

		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		
		return 1;
	} 
	for (i = 0; i < argc; i++) {
		
		//Create window	
		if(strcmp(args[i], "-f")) {
			
			SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN, &window, &renderer);
		
		} else {
		
			SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_FULLSCREEN_DESKTOP, &window, &renderer);
		}
	}

	if (window == NULL) { 
		
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		
		return 1;
	}

	//create the screen sruface where all the elemnts will be drawn onto (ball, paddles, net etc)
	screen = SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, SDL_PIXELFORMAT_RGBA32);
	
	if (screen == NULL) {
		
		printf("Could not create the screen surfce! SDL_Error: %s\n", SDL_GetError());

		return 1;
	}

	//create the screen texture to render the screen surface to the actual display
	screen_texture = SDL_CreateTextureFromSurface(renderer, screen);

	if (screen_texture == NULL) {
		
		printf("Could not create the screen_texture! SDL_Error: %s\n", SDL_GetError());

		return 1;
	}
	
		//Load the numbermap image
	playermap = SDL_LoadBMP("playermap.bmp");

	if (playermap == NULL) {
		
		printf("Could not Load playermap image! SDL_Error: %s\n", SDL_GetError());

		return 1;
	}
	background = SDL_LoadBMP("background.bmp");

	if (background == NULL) {
		
		printf("Could not Load background image! SDL_Error: %s\n", SDL_GetError());

		return 1;
	}
	// Set the title colourkey. 
	Uint32 colorkey = SDL_MapRGB(playermap->format, 255, 0, 255);
	SDL_SetColorKey(playermap, SDL_TRUE, colorkey);
	
	return 0;
}
